// const { DateTime } = require("luxon");
const pluginRss = require("@11ty/eleventy-plugin-rss");
const svgSprite = require("eleventy-plugin-svg-sprite");
const dateFilter = require("./src/filters/dateFilter.js");
const cleanCSS = require("clean-css");
//const eleventyWebcPlugin = require("@11ty/eleventy-plugin-webc");
//const { eleventyImagePlugin } = require("@11ty/eleventy-img");
const Image = require('@11ty/eleventy-img');

module.exports = function (config) {
  config.setServerOptions({
    // Whether the live reload snippet is used
    liveReload: true,
    port: 3456,
    watch: ["dist/**/*.css"],
    showAllHosts: true,
  });
  // PASSTHROUGHS
  //config.addPassthroughCopy("src/assets/images/");
  config.addPassthroughCopy("src/assets/fonts/");

  // LAYOUTS //
  config.addLayoutAlias("base", "layouts/base.njk");
  config.addLayoutAlias("post", "layouts/post.njk");

  // FILTERS //
  // date filter
  config.addFilter("dateFilter", dateFilter);
  // clean and inline CSS
  config.addFilter("cssmin", function (code) {
    return new cleanCSS({}).minify(code).styles;
  });

  // TRANSFORMS //
  // minify HTML
  const htmlMinTransform = require("./src/transforms/html-min.js");
  const isProduction = process.env.ELEVENTY_ENV === "production";
  // html min only in production
  if (isProduction) {
    config.addTransform("htmlmin", htmlMinTransform);
  }

  // PLUG-INS //
  config.addPlugin(pluginRss);
  config.addPlugin(svgSprite, {
    path: "./src/assets/icons",
    svgShortcode: "icon",
    globalClasses: "icon",
  });
  // WebC
	// config.addPlugin(eleventyWebcPlugin, {
	// 	components: [
	// 		// …
	// 		// Add as a global WebC component
	// 		"npm:@11ty/eleventy-img/*.webc",
	// 	]
	// });
	// Image plugin
	// config.addPlugin(eleventyImagePlugin, {
	// 	// Set global default options
	// 	formats: ["webp", "jpeg"],
	// 	urlPath: "/assets/images/",
  //   outputDir: "./dist/assets/images/",

	// 	// Notably `outputDir` is resolved automatically
	// 	// to the project output directory

	// 	defaultAttributes: {
	// 		loading: "lazy",
	// 		decoding: "async"
	// 	}
	// });

  // SHORTCODES
  config.addShortcode("image", async function(src, cls, alt, sizes) {
		let metadata = await Image(src, {
			widths: [300, 600],
			formats: ["webp", "jpeg", "png"],
      urlPath: "/assets/images/",
      outputDir: "dist/assets/images/",
		});

		let imageAttributes = {
      class: cls,
			alt,
			sizes,
			loading: "lazy",
			decoding: "async",
		};

		// You bet we throw an error on a missing alt (alt="" works okay)
		return Image.generateHTML(metadata, imageAttributes);
	});

  // EXTRAS //
  // Post List Excerpts
  config.setFrontMatterParsingOptions({
    excerpt: true,
    excerpt_separator: "<!-- excerpt -->",
  });

  // BASE CONFIGURATION //
  return {
    dir: {
      input: "src",
      output: "dist",
      includes: "includes",
      data: "data",
    },
    templateFormats: ["html", "njk", "md", "11ty.js"],
    htmlTemplateEngine: "njk",
    markdownTemplateEngine: "njk",
  };
};
